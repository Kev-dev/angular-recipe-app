import { Component } from '@angular/core';
import { Response } from '@angular/http';

import { AuthService } from '../../auth/auth.service';
import { RecipeApiService } from '../../shared/recipe-api.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent {
  constructor(private recipeApi: RecipeApiService, private authService: AuthService) { }
  onSaveRecipes() {
    this.recipeApi.storeRecipes().subscribe((response: Response) => {
      console.log(response);
    })
  }

  onFetchRecipes() {
    this.recipeApi.getRecipes();
  }
}