import { AuthService } from '../auth/auth.service';
import { Injectable } from "@angular/core";
import { Response } from "@angular/http"
import { Recipe } from "../recipes/recipe.model";
import { RecipeService } from "../recipes/recipe.service";
import { HttpClient } from "@angular/common/http";


@Injectable()
export class RecipeApiService {
  constructor(private httpClient: HttpClient, private recipeService: RecipeService, private authService: AuthService) { }

  storeRecipes() {
    const token: string = this.authService.getToken();
    return this.httpClient.put("https://recipe-book-b1892.firebaseio.com/recipes.json?auth=" + token, this.recipeService.getRecipes());
  }

  getRecipes() {
    const token: string = this.authService.getToken();
    return this.httpClient.get<Recipe[]>("https://recipe-book-b1892.firebaseio.com/recipes.json?auth=" + token).subscribe((recipes: Recipe[]) => {
      this.recipeService.setRecipes(recipes);
    });
  }
}