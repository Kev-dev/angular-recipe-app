import { Directive, HostListener, Renderer2, ElementRef } from "@angular/core";


@Directive({
  selector: "[appDropDown]"
})


export class DropDownDirective {
  constructor(private renderer: Renderer2, private elRef: ElementRef) { }
  open = false;

  @HostListener("click") toggleOpen(eventData: Event) {
    if (!this.open) {
      this.renderer.addClass(this.elRef.nativeElement, 'open');
    } else {
      this.renderer.removeClass(this.elRef.nativeElement, 'open');
    }
    this.open = !this.open;
  }

  /*
    @HostBinding('class.open') isOpen = false;
    @HostListener('click) toggleOpen() {
      this.open = !this.open
    }
  */
}