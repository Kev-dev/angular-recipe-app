import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyBMpTb10tgwJuUdu7H8d7rzewXaqN_D3-Y",
      authDomain: "recipe-book-b1892.firebaseapp.com",
    })
  }
}
